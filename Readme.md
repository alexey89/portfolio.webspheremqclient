## The project is a wrapper over a low-level WebSphereMQ client.

The [WebSphereMQ](http://www-01.ibm.com/support/docview.wss?uid=swg24037500) client must be installed on the computer to build the project.

All code is written personally